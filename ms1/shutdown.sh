#===============================================================================
#USAGE : ./shutdown.sh
#DESCRIPTION : Power off your machine using an script.
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
echo "Shutdown command"
echo -n "Are you sure that you want to shutdown the computer? (Yes / No) "
read answer

if [ "$answer" = "Yes" ];
then
    shutdown now
elif [ "$answer" = "No" ];
then
    echo "Shutdown command cancelled"
else
    echo "Invalid Option!"
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
