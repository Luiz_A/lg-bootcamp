#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
printf "Menu: \n(1) Shutdown\n(2) Restart\n(3) Turn Screen \n(4) Change keyboard language\nOption: "
read option

case $option in
    1) bash shutdown.sh
    ;;

    2) bash restart.sh
    ;;

    3) printf "Screen menu: \n(1) Left\n(2) Right\n(3) Normal\n(4) Inverted\nOption: "
       read screen
       case $screen in
            1) bash turn-screen.sh left
            ;;
            2) bash turn-screen.sh  right
            ;;
            3) bash turn-screen.sh  normal
            ;;
            4) bash turn-screen.sh  Inverted
            ;;
        esac
    ;;

    4) echo "Keyboard language: "
       read language
       bash keyboard-language.sh $language
    ;;
esac
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
